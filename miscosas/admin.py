from django.contrib import admin
from .models import Post, Usuario, Item, Alimentador, Comentario

admin.site.register(Post)
admin.site.register(Item)
admin.site.register(Comentario)
admin.site.register(Usuario)
admin.site.register(Alimentador)

