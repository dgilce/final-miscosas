from django.db import models
from django.utils import timezone

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title



class Alimentador(models.Model):
    id_canal = models.CharField(max_length=256)
    titulo = models.CharField(max_length=64)
    enlace = models.URLField()
    votos_pos = models.IntegerField()
    votos_neg = models.IntegerField()

    def __str__(self):
        return self.id_canal



class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    id_video = models.CharField(max_length=256)
    titulo = models.CharField(max_length=64)
    enlace = models.URLField()
    descripcion = models.CharField(max_length=512)
    video = models.CharField(max_length=256)
    votos_pos = models.IntegerField()
    votos_neg = models.IntegerField()

    def __str__(self):
        return self.titulo



class Comentario(models.Model):
    autor = models.CharField(max_length=50)
    texto = models.TextField()
    item = models.ForeignKey(Item, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.autor + ": " + self.texto



class Usuario(models.Model):
    username = models.CharField(max_length=100, default="ejemplo")
    password = models.CharField(max_length=50, default="ejemplo")
    email = models.EmailField(max_length=100, default="ejemplo@ejemplo.com")
    comentario = models.ForeignKey(Comentario, on_delete=models.CASCADE, blank=True, null=True)
    items = models.ManyToManyField(Item, blank=True, null=True)
    titulo = models.CharField(max_length=300, default="Pagina del usuario")
    nvotos = models.PositiveSmallIntegerField(default=0)
    ncomentarios = models.PositiveSmallIntegerField(default=0)
