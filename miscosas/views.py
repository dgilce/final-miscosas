from django.shortcuts import render
from django.utils import timezone
from django.shortcuts import redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


from .models import Post
from .forms import PostForm
from .forms import UserForm

def principal_page(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    form = AuthenticationForm()
    return render(request, 'blog/principal.html', {'posts': posts, 'form': form})

def post_new(request):
    form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

def item_page(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    form = AuthenticationForm()
    return render(request, 'blog/principal.html', {'posts': posts, 'form': form})

def registrarse_page(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                do_login(request, user)
                return redirect('/registrarse')

        return redirect("/registrarse/")
    else:
        form = UserCreationForm()
        return render(request, 'blog/registrarse.html', {'form': form})


def login_user(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "users/login.html", {'form': form})


def logout_user(request):
    logout(request)
    message = "Esperamos verte pronto. Sigue navegando por la <a href="#">web</a>"
    return redirect("/")


def alimentadores_page(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    form = AuthenticationForm()
    return render(request, 'blog/item.html', {'posts': posts, 'form': form})


def usuarios_page(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    form = AuthenticationForm()
    return render(request, 'blog/item.html', {'posts': posts, 'form': form})





def informacion_page(request):
    form = AuthenticationForm()
    return render(request, 'blog/informacion.html', {'form': form})

